package training.androidkotlin.notepad

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View

// on utilise la RecyclerView :
class NoteListActivity : AppCompatActivity(), View.OnClickListener {
    // on définit les notes de type MutableList pour pouvoir ajouter ou modifier
    lateinit var notes: MutableList<Note>
    // on créé l'adapter qui sera initialisé dans le onCreate
    lateinit var adapter: NoteAdapter
    // on récupère le coordinateurLayout car on aura besoin pour le snackbar
    lateinit var coordinatorLayout : CoordinatorLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // afiche la vue au lancement de la classe NoteListActivity
        setContentView(R.layout.activity_note_list)

        // utilisation de la toolbar
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        // on affecte cette toolbar en remplacement de celle par defaut qui existerai
        setSupportActionBar(toolbar)

        // recuperation du btn add + ecoute des actions
        findViewById<FloatingActionButton>(R.id.create_note_fab).setOnClickListener(this)

        notes = loadNotes(this)
        adapter = NoteAdapter (notes, this)
        // on va travailler sur la RecyclerView,on la récupère à partir du Layout
        val recyclerView = findViewById<RecyclerView>(R.id.notes_recycler_view)
        // on initialise le Layout Manager pour dire qu'on va avoir un linearManager en vertical par defaut
        recyclerView.layoutManager = LinearLayoutManager(this)
        // on connecte le recyclerView à l'adapter
        recyclerView.adapter = adapter

        coordinatorLayout = findViewById<CoordinatorLayout>(R.id.coordinator_layout)


    }
    // fonction declenchée par finish dans saveNote() de NotedetailActivity qui prend trois parametres
    // on fait un check pour s'assurer d'avoir tout ce qu'il faut
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK || data == null){
            return
        }
        //on traite notre request edit note en faisant processEditNotResult qui recupere les donnees de l'intent
        when (requestCode) {
            // REQUEST_EDIT_NOTE est le request_code de l'appelant
            NoteDetailActivity.REQUEST_EDIT_NOTE -> processEditNotResult(data)
        }
    }
    // recupère les donnees de l'intent et met à jour la liste avec la fonction saveNote
    private fun processEditNotResult(data: Intent) {
        val noteIndex = data.getIntExtra(NoteDetailActivity.EXTRA_NOTE_INDEX, -1)
        //on teste l'action de data (procesEditNoteResult)
        when (data.action){
            // on récupère la note et on fait un save_note
            NoteDetailActivity.ACTION_SAVE_NOTE -> {
            val note = data.getParcelableExtra<Note>(NoteDetailActivity.EXTRA_NOTE)
            saveNote(note, noteIndex)
            }
            // on récupere la note et on la supprime
            NoteDetailActivity.ACTION_DELETE_NOTE -> {
                deleteNote(noteIndex)
            }
        }
    }
    // dans onClick lorsqu'on a recupérer notre view.tag, on sait qu'on est en train de travailler sur une note
    // et on connait sa position
    override fun onClick(view: View) {
        // on test le tag pour vérifier qu'il y a bien un element
        if (view.tag != null) {
            //on appelle la fonction showNoteDetail dans le onClick du listItem qui nous a appelé
            // comme Tag est un obect, il faut le caster en int
            showNoteDetail(view.tag as Int)
        } else {
            when(view.id) {
                R.id.create_note_fab -> createNewNote()
            }
        }
    }


    //fonction enregistrement effectif
    // mettre à jour ma liste de note à partir de la note qui a ete passé en parametre avec l'indice fournit
    fun saveNote(note: Note, noteIndex: Int){
        persistNote(this, note)
        // ajout d'une nouvelle note à notre tableau
        if (noteIndex < 0) {
            notes.add(0, note)
        }else {
            notes[noteIndex] = note
        }
        adapter.notifyDataSetChanged()
    }

    private fun deleteNote(noteIndex: Int) {
        // on test l'indice pour être sûr de supprimer une note qui existe
        if (noteIndex < 0){
            return // on ne fait rien
        }
        // on récupère la note qu'on supprime
        val note= notes.removeAt(noteIndex)
        deleteNote(this, note)
        // on met à jour l'adapter pour l'informer du changement
        adapter.notifyDataSetChanged()

        Snackbar.make(coordinatorLayout, "${note.title} supprimée", Snackbar.LENGTH_SHORT)
            .show()
    }

    // fonction createNewNote autour du wraper
    fun createNewNote() {
        showNoteDetail(-1)
    }

    // fonction pour afficher une note, elle va recuperer la note dans notre tableau note et va
    // lancer noteDetailActivity en passant Note en paramètre
    fun showNoteDetail(noteIndex: Int){
        // si note est inférieur à 0 on crée une nouvelle note, si non, on prend l'index de la note
        val note = if (noteIndex <0) Note() else notes[noteIndex]
        // quand on lance une activity, on lui fournit des informations avec les extra de notre intent
        val intent = Intent(this, NoteDetailActivity::class.java)
        intent.putExtra(NoteDetailActivity.EXTRA_NOTE, note as Parcelable)
        intent.putExtra(NoteDetailActivity.EXTRA_NOTE_INDEX, noteIndex)
        // on demarre l'activity et on attend un resultat de cette acitivty en disant qu'on veut une edition d'une note
        startActivityForResult(intent, NoteDetailActivity.REQUEST_EDIT_NOTE)

    }
}



