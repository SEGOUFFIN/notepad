package training.androidkotlin.notepad

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment


// on hérite de DialogFragment (V4 de la librairie de support)
// l'unique paramètre passé au constructeur sera le titre de la note

class ConfirmDeleteNoteDialogFragment @SuppressLint("ValidFragment") constructor(val noteTitle: String = "") : DialogFragment() {

    // preparation de l'interface pour pouvoir notifier l'appelant du clic sur Oui ou Non sur notre dialog

    interface ConfirmDeleteDialogListener {
        // contenant uniquement 2 fonctions
        fun onDialogPositiveClick()
        fun onDialogNegativeClick()
    }

    // on aura un listener qui pourra etre attaché ou non
    var listener: ConfirmDeleteDialogListener? = null

    // surcharge de la fonction onCreate
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // l'implémentation
        // on déclare un builder à partir d'un alertdialog.builder en ajoutant le contexte de l'activity
        //qui nous appelera
        val builder = AlertDialog.Builder(activity)

        // on va construire notre dialogue en commencant par le message

        builder.setMessage("Êtes vous sûr de supprimer cette note \"$noteTitle\" ?")
            .setPositiveButton("Supprimer",
                DialogInterface.OnClickListener { dialog, id -> listener?.onDialogPositiveClick()})
            .setNegativeButton("Annuler",
                DialogInterface.OnClickListener { dialog, id -> listener?.onDialogNegativeClick()})

        // on retourne un builder.create
        return builder.create()
    }
}