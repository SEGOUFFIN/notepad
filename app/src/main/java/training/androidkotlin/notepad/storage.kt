package training.androidkotlin.notepad

import android.content.Context
import android.text.TextUtils
import android.util.Log
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.*

// message de TAG pour les eventuels messages que l'on va mettre
private val TAG = "storage"

// fonction qui va permettre de persister les donnees. en parametre elle prend un context et la note que l'on veut persister
fun persistNote(context: Context, note: Note){
    // si il n'y a pas de nom de fichier attribué, on le génère automatiquement
    if (TextUtils.isEmpty(note.fileName)) {
        //la classe UUID permet de générer des identifiants uniques (nombres) + utilisation de la fonction toString pour convertir
        note.fileName = UUID.randomUUID().toString() + ".note"
    }

    Log.i(TAG, "Saving note $note")
    //la serialisation va se faire en ouvrant un fichier en écriture, don on créé une variable fileOutput
    //ici, context nous donne accés à des fichiers qui seront stockés dans le dossier unique de notre application
    val fileOutput = context.openFileOutput(note.fileName, Context.MODE_PRIVATE)
    // ouverture d'un flux de données pour prendre notre objet Serializable et l'ecrire dans notre fichier
    //le flux de donnee se fait avec ObjectOutputStream (fonction JAVA) qui prend en parametre
    // un outputstream (notre fileOutput qu'on vient de créer) et qu'on stocke dans une variable
    val outputStream = ObjectOutputStream(fileOutput)
    // à partir de là, on peut ecrire dans notre outputFile
    outputStream.writeObject(note)
    // on cloture notre Stream
    outputStream.close()
}


fun loadNotes(context: Context) : MutableList<Note> {
    val notes = mutableListOf<Note>()

    val notesDir = context.filesDir
    for (filename in notesDir.list()) {
        val note = loadNote(context, filename)
        Log.i(TAG, "loaded note $note")
        notes.add(note)
    }

    return notes
}


fun deleteNote (context: Context, note: Note) {
    context.deleteFile(note.fileName)
}


// fonction qui va charger un fichier passé en paramètre
private fun loadNote(context: Context, filename: String) : Note {
    val fileInput = context.openFileInput(filename)
    val inputStream = ObjectInputStream(fileInput)
    val note = inputStream.readObject() as Note
    inputStream.close()

    return note

}