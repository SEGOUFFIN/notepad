package training.androidkotlin.notepad

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable


// definition de la class Note
// definition des variables membres de note (titre, texte et nom de fichier)
// toutes les variables sont en var car elles pourront évoluer
// notre class est Parcelable pour être transférée entre plusieurs Activity,
// et Serializable pour stocker nos objets (Note) en fichier, item ou autre ...
data class Note (var title: String = "",
                 var text: String = "",
                 var fileName: String = "" ) : Parcelable, Serializable {
    // implementation de Parcelable + génération automatique constructor + fun
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()){
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(text)
        parcel.writeString(fileName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Note> {
        // serailVersionUid = identifiant unique qui va permettre à la JVM de reconnaitre
        // les versions de notre class Note suite à l'ajout de Serializable
        private val serialVersionUid: Long = 987654321987
        override fun createFromParcel(parcel: Parcel): Note {
            return Note(parcel)
        }

        override fun newArray(size: Int): Array<Note?> {
            return arrayOfNulls(size)
        }
    }
}

// 1ere passe terminée sur la class note, on va pouvoir l'utiliser pour l'afficher dans une liste