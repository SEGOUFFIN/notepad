package training.androidkotlin.notepad


import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

// on passe deux parametres au constructeur de notre adapter, la liste de note et
//lelistener pour notifier notre appelant des clicks fait sur les differents items du NoteAdapter
class NoteAdapter (val notes: List<Note>, val itemClickListener: View.OnClickListener)
    //on definit l'heritage en appellant le super constructeur de notre classe parent RecyclerView.Adapter
    // et entre chevron, il attend le ViewHolder dont on définie la class ci dessous
    : RecyclerView.Adapter<NoteAdapter.ViewHolder>(){
    // definition class Viewholder pour le superconstructeur
    // il prend en paramètre la vue sur laquelle il va se lier et on appelle la classe parent RecyclerView et on passe
    // en parametre itemView au superconstructeur
    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        // on définit les differentes vue sur lequel le View holder va se lier pour pouvoir les garder en memoire
        val cardView = itemView.findViewById(R.id.card_view) as CardView
        //à partir de là on peut récupérer notre titre et notre extrait
        val titleView = cardView.findViewById(R.id.title) as TextView
        val excerptView = cardView.findViewById(R.id.excerpt) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem= LayoutInflater.from(parent.context)
            .inflate(R.layout.item_note, parent, false)
        //cette opération nous aura permis de convertir le Layout itemNote en objet kotlin que l'on peut manipuler
        // on renvoi une instance de Viewholder en passant l'objet Kotlin viewItem en parametre
        return ViewHolder(viewItem)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // on va lier nos data par rapport à la vue qui est stockée dans le holder
        // on recupere la data qui est une note qu'on récupère à la position passée en parametre dans notre liste
        val note = notes[position]
        // on met à jour nos champs dans le Viewholder en commencant par cardView et
        // on definit le onClick listener par rapport
        holder.cardView.setOnClickListener(itemClickListener)
        // on definit le Tag pour pouvoir retouver notre note dans la liste
        holder.cardView.tag = position
        // on mets à jour les 2 champs texte
        holder.titleView.text = note.title
        // là on peut mettre tout le texte car dans le layout, on a definit que excerpt ne prendrait qu'une ligne et 3 ...
        holder.excerptView.text = note.text
    }
    // retourne le nombre d'éléments dans notre liste
    override fun getItemCount(): Int {
        return notes.size
    }


}