package training.androidkotlin.notepad

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView


class NoteDetailActivity : AppCompatActivity() {

    // Affichage d'une note qu'on aura reçu via un itent
    // définition des clés des extra qu'on va chercher dans notre intent

    // companion object pour pouvoir les referencer en tant que constante
    // depuis notre listActivity pour garder la definition à un seul endroit
    companion object {
        val REQUEST_EDIT_NOTE = 1
        val EXTRA_NOTE = "note"
        val EXTRA_NOTE_INDEX = "noteIndex"

        val ACTION_SAVE_NOTE = "training.androidkotlin.notepad.actions.ACTION_SAVE_NOTE"
        val ACTION_DELETE_NOTE = "training.androidkotlin.notepad.actions.ACTION_DELETE_NOTE"
     }
    // champ qui vont etres associes aux valeurs que l'on recupere de ces extra
    // lateinit var note qui sera récupéré dans le onCreate
    lateinit var  note: Note
    // noteIndex qui ne peut pas etre de type lateinit car c'est un type primitif
    var noteIndex: Int = -1

    // titleView et textView à récupérer du Layout, à initialiser à partir des données de note
    // variables membres
    lateinit var titleView: TextView
    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)

        //déclarer et utiliser la toolbar
       val toolbar = findViewById<Toolbar>(R.id.toolbar)
       setSupportActionBar(toolbar)
        // afficher la fleche "Back" dans la Toolbar de NoteDetailActivity
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        // initialisation des deux variables membres à partir de l'intent qui nous a appelé
        // intent = action que l'on veut executer et getParcelableExtra = methode de intent pour recuperer la donnée
        // qui sera casté en Note avec la clé extra_note
        note = intent.getParcelableExtra<Note>(EXTRA_NOTE)
        // si note index est à -1, c'est une nouvelle note, si non ce sera l'index du tableau jusqu'à la valeur maxi -1
        noteIndex = intent.getIntExtra(EXTRA_NOTE_INDEX, -1)

        // on récupère du Layout titleView et textView
        titleView = findViewById<TextView>(R.id.title)
        textView = findViewById<TextView>(R.id.text)

        //on arécupérer tous nos champs, il reste à initialiser titleView et textView à partir du contenu de Note
        titleView.text = note.title
        textView.text = note.text

    }
    // activation du menu activity_note_detail en surchargent la fonction onCreateOptionMenu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // permet de récupérer le menu créé dans menu/note_detail_activity
        menuInflater.inflate(R.menu.activity_note_detail, menu)
        return true
    }
    // on ajoute les items par rapport au point d'entrée de ce menu (actio save)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action sur btn save
            R.id.action_save -> {
                saveNote()
                return true
            }
            // action sur btn delete
            R.id.action_delete -> {
                showConfirmDeleteNoteDialog()
                return true
            }
            //comportement par defaut
            else -> return super.onOptionsItemSelected(item)
        }
    }
    //fonction suppression dialog btn delelte
    private fun showConfirmDeleteNoteDialog() {
        // on instancie notre dialog
        val confirmFragment = ConfirmDeleteNoteDialogFragment(note.title)
        confirmFragment.listener = object : ConfirmDeleteNoteDialogFragment.ConfirmDeleteDialogListener {
            // dialog de confirmation de suppression
            override fun onDialogPositiveClick() {
            // on appelle la fonction pour supprimer la note
                deleteNote()
            }
            // pour la négative, on ne fait rien
            override fun onDialogNegativeClick() {}
        }
        confirmFragment.show(supportFragmentManager, "confirmDeleteDialog")
    }

    fun saveNote(){
        // on recupere les donnees pour mettre à jour notre note
        note.title = titleView.text.toString()
        note.text = textView.text.toString()
        // on crée un intent pour mettre toutes les informations de cette note qui a été mise à jour
        // ainsi que l'index de la note dans la liste passée en paramètre dans le onCreate
        intent = Intent(ACTION_SAVE_NOTE)
        intent.putExtra(EXTRA_NOTE, note as Parcelable)
        intent.putExtra(EXTRA_NOTE_INDEX, noteIndex)
        // on fait un setresult ok et on passe nos donnees en parametres
        setResult(Activity.RESULT_OK, intent)
        // on appelle le finish pour terminer notre activity qui va declencher en retour de notre NoteListActivity
        // l'appel de la fonction onActivityResult
        finish()
    }
    // cette fonction sera applelée lors du click sur ok lors de la dialog de confirmation
    fun deleteNote() {
        // on créé un intent pour la suppression
        intent = Intent(ACTION_DELETE_NOTE)
        // on ajoute en parametre un extra qui correspond à l'indice de la note à supprimer
        intent.putExtra(EXTRA_NOTE_INDEX, noteIndex)
        setResult(Activity.RESULT_OK, intent)
        finish()

        // lorsqu'on fait un setResult() et un finish(), on retourne sur NoteListActivity dans notre
        // fonction onActivityResult


    }
}
